# boe-demo

```js

npm install -g commitizen
npm install -g conventional-changelog
npm install -g conventional-changelog-cli

// 运行下面命令，使其支持Angular的Commit message格式
commitizen init cz-conventional-changelog --save --save-exact

// 生成CHANGELOG
conventional-changelog -p angular -i CHANGELOG.md -s

// commitlint 校验提交
npm i --save-dev @commitlint/config-conventional @commitlint/cli

// 在项目根目录创建 commitlint.config.js 文件并设置校验规则
// module.exports = {
//   extends: ["@commitlint/config-conventional"],
//   // rules 里面可以设置一些自定义的校验规则
//   rules: {},
// };
// 安装 husky
npm install --save-dev husky

// husky.config.js 中加入以下代码
// module.exports = {
//   "hooks": {
//     "commit-msg": "commitlint -E HUSKY_GIT_PARAMS"
//   }
// }

```


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
